<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:twitter="http://dev.twitter.com/cards#"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="fr"
      >
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <meta name="twitter:site" content="@iridl" />
<title>Ma Première Map Room</title>
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<link rel="shortcut icon" href="../uicore/icons/iri32.png" />
<link rel="apple-touch-icon" sizes="54x54" href="../uicore/icons/iriwh.png" />
<link rel="icon" href="../uicore/icons/iriwh.svg" sizes="any" type="image/svg+xml" />
      <meta property="maproom:Sort_Id" content="a01" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/ds:/SOURCES/.NOAA/.NCEP/.CPC/.GLOBAL/.monthly/.olr/X/Y/1/SM121/DATA/10/STEP/Y/%2822S%29%2822N%29RANGE//name//tropical_OLR/def/SOURCES/.NOAA/.NCEP/.CPC/.GLOBAL/.monthly/.olr/X/Y/1/SM121/DATA/10/STEP//name//global_OLR/def/:ds/a-+.tropical_OLR+startcolormap+DATA+120+300+RANGE+5+130+5+RGB+RGBdup+120+VALUE+5+130+5+RGB+RGBdup+170+VALUE+10+150+15+RGB+RGBdup+180+VALUE+20+170+25+RGB+RGBdup+190+VALUE+45+180+50+RGB+RGBdup+200+VALUE+70+190+75+RGB+RGBdup+210+VALUE+110+210+115+RGB+RGBdup+220+VALUE+150+230+155+RGB+RGBdup+230+VALUE+170+242+175+RGB+RGBdup+240+VALUE+211+211+211+RGB+RGBdup+250+VALUE+255+255+255+RGB+300+VALUE+white+endcolormap+-a-+.global_OLR+-a+X+Y+fig:+colors+grey+unlabelledcontours+black+thin+solid+coasts+countries+:fig+/T/last/plotvalue/X/-20/340/plotrange/Y/-65/75/plotrange//plotborder+0+psdef//antialias+true+psdef//XOVY+null+psdef+.gif" />
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
</head>

<body>

<form name="pageform" id="pageform">
<input class="carryLanguage" name="Set-Language" type="hidden" />
<input class="titleLink itemImage bodyAttribute" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem">
                <legend>Mon Univers</legend> 
                      <a rev="section" class="navlink carryup carryLanguage" href="/">Data Library</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Data Library</legend> 
                     <span class="navtext">Ma Première Maproom</span>

            </fieldset> 
            <fieldset class="navitem"> 
                <legend>Région</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregions.json"></a>
		<select class="RegionMenu" name="bbox">
		<option value="">Global</option>
		<optgroup class="template" label="Region">
		<option class="irigaz:hasPart irigaz:id@value term:label"></option>
		</optgroup>
		</select>
            </fieldset>
 </div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Ma Première Map Room</h2>
<p align="left" property="term:description">
Cette Maproom est un tutoriel qui va vous enseigner comment fabriquer des Maprooms.
</p>
</div>

<div class="rightcol tabbedentries" about="/maproom/" >
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Partager</legend>

</fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>

 </html>
